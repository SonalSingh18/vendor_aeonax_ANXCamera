# ANXCamera
## Getting Started :
### Cloning :
- Clone this repo in vendor/aeonax/ANXCamera in your working directory by :
```
git clone -b eleven https://gitlab.com/SonalSingh18/vendor_aeonax_ANXCamera vendor/aeonax/ANXCamera
```
### Changes Required :
- You will need [these changes in your device tree.](https://github.com/ArrowOS-Devices/android_device_xiaomi_sm6250-common/commit/e1885588a7a023d21611c04a17ad6278443f3ea5)
- Done, continue building your ROM as you do normally.
